#!/usr/bin/env python

from pprint import pprint
import boto3
import re
import argparse

parser = argparse.ArgumentParser(description='Find out interesting information on AWS Security Groups')
parser.add_argument("-o", default=None, type=str, help="Output format, default: cli")
parser.add_argument("-s", default='unused', type=str,
                    choices=["unused", "open", "ec2-classic"],
                    help="Data type, default: unused")

args = parser.parse_args()

client_ec2 = boto3.client('ec2')

classic_insts = list(client_ec2.describe_classic_link_instances()['Instances'])

for inst in classic_insts:
    print inst['InstanceId']
    print inst['VpcId']
    for group in inst['Groups']:
        print group['GroupId']
    print ""
