#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
    Created By:  Mark Lin (mark@kloktech.com)
    Description: Check AWS security groups for cleanup and vulnerability issues.
'''

from pprint import pprint
import boto3
import re
import argparse
from IPy import IP

parser = argparse.ArgumentParser(description='Find out interesting information on AWS Security Groups')
parser.add_argument("--output", default='cli', type=str, choices=["cli", "csv"], help="Output type, default: cli")
parser.add_argument("--type", default='unused', type=str,
                    choices=["unused", "open", "ec2-classic", "external-ip"],
                    help="Data type, default: unused")
parser.add_argument('--dependent', dest='dependent', action='store_true', help="Counting dependent SG in unused case.")
parser.add_argument('--no-dependent', dest='dependent', action='store_false', help="Not counting dependent SG in unused case")
parser.set_defaults(dependent=True)

args = parser.parse_args()


ec2 = boto3.resource('ec2')
client_ec2 = boto3.client('ec2')
client_elb = boto3.client('elb')
client_elbv2 = boto3.client('elbv2')
client_rds = boto3.client('rds')
client_redshift = boto3.client('redshift')
client_elasticache = boto3.client('elasticache')

if (args.output == 'cli'):
    class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'
else:
    class bcolors:
        HEADER = ''
        OKBLUE = ''
        OKGREEN = ''
        WARNING = ''
        FAIL = ''
        ENDC = ''
        BOLD = ''
        UNDERLINE = ''


sgs = list(ec2.security_groups.all())
insts = list(ec2.instances.all())
classic_insts = list(client_ec2.describe_classic_link_instances()['Instances'])
elbs = list(client_elb.describe_load_balancers()['LoadBalancerDescriptions'])
elbsv2 = list(client_elbv2.describe_load_balancers()['LoadBalancers'])
rds = list(client_rds.describe_db_instances()['DBInstances'])
redshifts = list(client_redshift.describe_clusters()['Clusters'])
elasticaches = list(client_elasticache.describe_cache_clusters()['CacheClusters'])

elb_sgs = set() # Unique value only in set
elb_sgs_details = {}
sgs_details = {}
sgs_open_ports = {} # Security group with open ports
sgs_dependencies = {} # SG with other SG in source
sgs_reverse_dependencies = {} # SG in other SG
inst_ips = {}
inst_ids = {}
ec2_classic_in_sg = set()
inst_sgs = {}
classic_inst_sgs = {}
external_addresses = {}

# Create instanes' ip to id mapping
for inst in insts:
    inst_ips[inst.public_ip_address] = inst.id
    for tag in inst.tags:
        if tag['Key'] == 'Name':
            name = tag['Value']
    inst_ids[inst.id] = inst
    for sg in inst.security_groups:
        if sg['GroupId'] not in inst_sgs:
            inst_sgs[sg['GroupId']] = set()
        inst_sgs[sg['GroupId']].add(inst.id + '(' + name + ')')

# Get names of all sgs
all_sgs = set([sg.group_id for sg in sgs])

# for sg in sgs:
#     print sg

# Get mapping of name to id
for sg in sgs:
    sgs_details[sg.group_id] = sg

# elbsv
for elb in elbs:
    elb_sgs.update(set(elb['SecurityGroups']))
    for sg in elb['SecurityGroups']:
        if sg not in elb_sgs_details:
            elb_sgs_details[sg] = set()
        elb_sgs_details[sg].add(elb['LoadBalancerName'])
# elbsv2 = alb
for elb in elbsv2:
    elb_sgs.update(set(elb['SecurityGroups']))
    for sg in elb['SecurityGroups']:
        if sg not in elb_sgs_details:
            elb_sgs_details[sg] = set()
        elb_sgs_details[sg].add(elb['LoadBalancerName'])

# RDS security groups
rds_sgs = set([sg['VpcSecurityGroupId'] for db in rds for sg in db['VpcSecurityGroups']])
rds_sgs_details = {}
for db in rds:
    for sg in db['VpcSecurityGroups']:
        if sg['VpcSecurityGroupId'] not in rds_sgs_details:
            rds_sgs_details[sg['VpcSecurityGroupId']] = set()
        rds_sgs_details[sg['VpcSecurityGroupId']].add(db['DBInstanceIdentifier'])

# Redshift security groups
redshift_sgs = set([sg['VpcSecurityGroupId'] for cluster in redshifts for sg in cluster['VpcSecurityGroups']])
redshift_sgs_details = {}
for db in redshifts:
    for sg in db['VpcSecurityGroups']:
        if sg['VpcSecurityGroupId'] not in redshift_sgs_details:
            redshift_sgs_details[sg['VpcSecurityGroupId']] = set()
        redshift_sgs_details[sg['VpcSecurityGroupId']].add(db['ClusterIdentifier'])

# EC2 usage
all_inst_sgs = set([sg['GroupId'] for inst in insts for sg in inst.security_groups])

# Classic instance
classic_inst_sgs = set([group['GroupId'] for inst in classic_insts for group in inst['Groups']])

# elasticache
elasticache_sgs = set([sg['SecurityGroupId'] for elasticache in elasticaches for sg in elasticache['SecurityGroups']])
elasticache_sgs_details = {}
for elasticache in elasticaches:
    for sg in elasticache['SecurityGroups']:
        if sg['SecurityGroupId'] not in elasticache_sgs_details:
            elasticache_sgs_details[sg['SecurityGroupId']] = set()
        elasticache_sgs_details[sg['SecurityGroupId']].add(elasticache['CacheClusterId'])

def gatherDependencies(sgs):
    for sg in sgs:
        for ip_permission in sgs_details[sg].ip_permissions:
            port_range = ''
            # In case of all protocol
            if str(ip_permission['IpProtocol']) == 'icmp':
                port_range = '7'
            elif str(ip_permission['IpProtocol']) != '-1':
                if ip_permission['FromPort'] != ip_permission['ToPort']:
                    port_range = str(ip_permission['FromPort']) + "-" + str(ip_permission['ToPort'])
                else:
                    port_range = str(ip_permission['FromPort'])
            else:
                port_range = 'ALL Protocol'

            # Show IpRange that's allow for those access
            for ip_range in ip_permission['IpRanges']:
                # Find ports open to the world and record them for each SG
                if ip_range['CidrIp'] == '0.0.0.0/0' or ip_range['CidrIp'] == '::/0':
                    if sg not in sgs_open_ports:
                        sgs_open_ports[sg] = set()
                    sgs_open_ports[sg].add(port_range)

                # check for IP that's associated with EC2 Instances
                ip = re.sub('\/.*$', '', ip_range['CidrIp'])
                if ip in inst_ips:
                    # Add instance ip to the list
                    ec2_classic_in_sg.add(sg)

                # check if IP is external
                check_ip = IP(ip)
                if check_ip.iptype() == 'PUBLIC':
                    if sg not in external_addresses:
                        external_addresses[sg] = set()
                    if 'Description' in ip_range:
                        external_addresses[sg].add(ip + '(' + ip_range['Description'] + ')')
                    else:
                        external_addresses[sg].add(ip)

            # Show Security Groups allow for those access
            # Will also map out dependency here
            for user_id_group_pair in ip_permission['UserIdGroupPairs']:

                # Skip self depending SG.
                if sg != user_id_group_pair['GroupId']:

                    # Record dependency
                    if sg not in sgs_dependencies:
                        sgs_dependencies[sg] = set()
                    if user_id_group_pair['GroupId'] not in sgs_reverse_dependencies:
                        sgs_reverse_dependencies[user_id_group_pair['GroupId']] = set()

                    sgs_dependencies[sg].add(user_id_group_pair['GroupId'])
                    sgs_reverse_dependencies[user_id_group_pair['GroupId']].add(sg)

gatherDependencies(all_sgs)

# Find list of unused SG by removing SG from instance, elb, etc...
unused_sgs = all_sgs - all_inst_sgs - classic_inst_sgs - elb_sgs - rds_sgs - redshift_sgs - ec2_classic_in_sg - elasticache_sgs

if args.type == 'unused':
    if args.dependent:
        print(bcolors.HEADER + 'Following SG is determined not being used by other services but depended by other SG:' + bcolors.ENDC)
        print('id,name,external IPs,depend by')
        for unused_sg in unused_sgs:
            if sgs_details[unused_sg].group_name != 'default':
                external_ips = ','
                if unused_sg in external_addresses:
                    external_ips += '"' + ", ".join(external_addresses[unused_sg]) + '"'
                depended_by = ','
                if unused_sg in sgs_reverse_dependencies:
                    depended_by += '"' + ", ".join(sgs_reverse_dependencies[unused_sg]) + '"'
                print(bcolors.OKBLUE + unused_sg + bcolors.ENDC + ',' + sgs_details[unused_sg].group_name + external_ips + depended_by)
    else:
        print(bcolors.HEADER + 'Following SG is determined not being used by other services and NOT depended by other SG, hence deletable:' + bcolors.ENDC)
        print('id,name,external IPs')
        for unused_sg in unused_sgs:
            if sgs_details[unused_sg].group_name != 'default':
                if unused_sg not in sgs_reverse_dependencies:
                    external_ips = ','
                    if unused_sg in external_addresses:
                        external_ips += '"' + ", ".join(external_addresses[unused_sg]) + '"'
                    print(bcolors.OKBLUE + unused_sg + bcolors.ENDC + ',' + sgs_details[unused_sg].group_name + external_ips)

if (args.type == 'open'):
    print(bcolors.HEADER + 'Following SG has ports open to world:' + bcolors.ENDC)
    print('id,name,inUse,VPC,ports,external IP,ec2 ,elb, rds, redhisft, elasticache')
    for sg in sgs:
        if sg.group_id in sgs_open_ports:
            in_use = bcolors.OKBLUE + 'N' + bcolors.ENDC
            if sg.group_id not in unused_sgs:
                in_use = bcolors.WARNING + 'Y' + bcolors.ENDC

            if sg.vpc_id == None:
                vpc_id = 'No VPC'
            else:
                vpc_id = sg.vpc_id

            # Output external ip if found
            external_ips = ''
            if sg.group_id in external_addresses:
                external_ips =  ", ".join(external_addresses[sg.group_id])
            # Find services using the security groups
            ec2_used = ""
            if sg.group_id in inst_sgs:
                ec2_used = ", ".join(inst_sgs[sg.group_id])

            elb_used = ""
            if sg.group_id in elb_sgs_details:
                elb_used = ", ".join(elb_sgs_details[sg.group_id])

            rds_used = ""
            if sg.group_id in rds_sgs_details:
                rds_used = ", ".join(rds_sgs_details[sg.group_id])

            redshift_used = ""
            if sg.group_id in redshift_sgs_details:
                redshift_used = ", ".join(redshift_sgs_details[sg.group_id])

            elasticache_used = ""
            if sg.group_id in elasticache_sgs_details:
                elasticache_used = ", ".join(elasticache_sgs_details[sg.group_id])

            print(bcolors.OKBLUE + sg.group_id + bcolors.ENDC + ',' + sg.group_name + ',' + in_use + ',"' + vpc_id + '","' + ', '.join(sgs_open_ports[sg.group_id]) + '","' + external_ips + '","' + ec2_used + '","' + elb_used + '","' + rds_used + '","' + redshift_used + '","' + elasticache_used + '"')

if (args.type == 'ec2-classic'):
    print(bcolors.HEADER + 'Following SG references to ec2 classic IP:' + bcolors.ENDC)
    print('id,name')
    for sg in ec2_classic_in_sg:
        print(bcolors.OKBLUE + sg + bcolors.ENDC + ',' + sgs_details[sg].group_name)

if (args.type == 'external-ip'):
    print(bcolors.HEADER + 'Following SG references to external IPs:' + bcolors.ENDC)
    print('id,ips')
    for sg in external_addresses:
        print(bcolors.OKBLUE + sg + '(' + sgs_details[sg].group_name + ')' + bcolors.ENDC + ',"' + ", ".join(external_addresses[sg]) + '"')
