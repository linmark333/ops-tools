Various tools for ops


```
# export following for boto3
# AWS_DEFAULT_REGION
# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
# or just export profile
# AWS_PROFILE


# Ouputs comma separated security group name and ID not used by ec2, elb(alb) and rds
check_unused_sg.py

# Output SG with open rules
./check_sg.py --type open --output csv > open.csv
# Output SG without usage and without dependency.  They can be deleted immediately without failure notice.
./check_sg.py --type unused --no-dependent --output csv > unused-sg-deletable.csv
# Output SG without usage and with dependency
./check_sg.py --type unused --output csv > unused-dependent.csv

```
